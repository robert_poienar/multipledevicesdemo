This is a just demo to demonstrate the base setup for AltUnity Tester to test multiple devices.   
This project contains 2 Windows builds to which AltUnity Tester will connect.  
Each build runs on a different port which is the most important thing.

If you have any question or trouble understanding the setup please write [Gitter](https://gitter.im/AltUnityTester/Lobby)

# Run the test:

1. Open both game(multipleDevice13000 && multipleDevice13000)
2. Open AltUnity Tester GUI 
3. Press "Run all test"


# Setup for test file

Test file can be find in Assets/Editor/NewAltUnityTest.cs
The only different things is that in constructor you instantiate 2 AltUnityDriver each one with one of the games port.

# Building the games

Just set a different server port before building the game. If you want to execute the 2 games in the same device then you also should change product name. Otherwise you will get an error when launching both game.

If you build for mobile device then there is an extra step. You have to port forward both of the devices which is easily done with AltUnity Tester GUI.


