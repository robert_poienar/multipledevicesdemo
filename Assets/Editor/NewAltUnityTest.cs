﻿using NUnit.Framework;

public class NewAltUnityTest
{
    public AltUnityDriver AltUnityDriver1;
    public AltUnityDriver AltUnityDriver2;
    //Before any test it connects with the socket
    [OneTimeSetUp]
    public void SetUp()
    {
        AltUnityDriver1 =new AltUnityDriver(tcp_port:13000);
        AltUnityDriver2 =new AltUnityDriver(tcp_port: 13001);
    }

    //At the end of the test closes the connection with the socket
    [OneTimeTearDown]
    public void TearDown()
    {
        AltUnityDriver1.Stop();
        AltUnityDriver2.Stop();
    }

    [Test]
    public void Test()
    {
        var altElement1 = AltUnityDriver1.FindObject(By.NAME, "Text");
        var altElement2 = AltUnityDriver2.FindObject(By.NAME, "Text");
        Assert.AreEqual("New Text", altElement1.GetText());
        Assert.AreEqual("New Text", altElement2.GetText());

    }

   

}